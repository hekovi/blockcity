package  
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import Block;
	/**
	 * ...
	 * @author Henrik
	 */
	public class CityGraphics extends Sprite
	{
		//textures:
		[Embed(source = "/../textures/Mountain_Rock_COLOR.png")] private var TextureRock:Class;
		[Embed(source = "/../textures/021metal.png")] private var TextureMetal:Class;
		
		[Embed(source = "/../textures/BlockIndustrial.png")] private var TextureBlockIndustrial:Class;
		[Embed(source = "/../textures/BlockPolice.png")] private var TextureBlockPolice:Class;
		[Embed(source = "/../textures/BlockResidential.png")] private var TextureBlockResidential:Class;
		[Embed(source = "/../textures/BlockSlum.png")] private var TextureBlockSlum:Class;
		[Embed(source = "/../textures/BlockTownhall.png")] private var TextureBlockTownhall:Class;
		
		//consts:
		private const gridSize:uint = 64;
		private const bShowGridlines:Boolean = true;
		private const gridColor:uint = 0xff6600;
		
		//private vars:
		private var gridLines:Sprite;
		private var hightlightShape:Shape;
		private var currentOverlayShape:Shape;
		
		
		public function CityGraphics() 
		{
		}
		
		public function Init():void
		{
			createGraphics();
			EnableCityInput(true);
		}
		
		public function EnableCityInput(inEnable:Boolean):void
		{
			if (inEnable)
			{
				this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_UP, handleMouse);
				this.addEventListener(MouseEvent.CLICK, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_OVER, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_OUT, handleMouse);
				this.addEventListener(MouseEvent.ROLL_OVER, handleMouse);
				this.addEventListener(MouseEvent.ROLL_OUT, handleMouse);
			}
			else
			{
				this.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_UP, handleMouse);
				this.removeEventListener(MouseEvent.CLICK, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_OVER, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_OUT, handleMouse);
				this.removeEventListener(MouseEvent.ROLL_OVER, handleMouse);
				this.removeEventListener(MouseEvent.ROLL_OUT, handleMouse);
			}
		}
		
		private function handleMouse(event:MouseEvent):void
		{
			//trace("Handling mouse event for: "+event.target +" of type: "+event.type);
			switch (event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					trace("CityGraphics Mouse down on:" +event.target);
					break;
					
				case MouseEvent.MOUSE_UP:
					trace("CityGraphics Mouse up on:" +event.target);
					break;
					
				case MouseEvent.CLICK:
					trace("CityGraphics Mouse CLICK on:" +event.target);
					trace("stage.mouseX:" +stage.mouseX);
					trace("stage.mouseY:" +stage.mouseY);
					var coordX:int = (int)(stage.mouseX / gridSize);
					var coordY:int = (int)(stage.mouseY / gridSize);
					trace("Coords X/Y: " + coordX + "/" + coordY);
					
					if (coordX < Reg.gameWidth && coordY < Reg.gameHeight)
					{
						var clickedBlock:Block = Reg.blockData[coordX][coordY];
						if (clickedBlock != null)
						{
							clickedBlock.changeType(Reg.selectedBlockType);
							createGraphics(); //TODO: Don't recreate ALL the graphics
						}
					}
					break;
					
				case MouseEvent.MOUSE_MOVE:
					break;
					
				case MouseEvent.MOUSE_OVER:
					trace("CityGraphics Mouse MOUSE_OVER on:" +event.target);
					if (event.target is GridGraphics)
					{
						currentOverlayShape.x = event.target.x;
						currentOverlayShape.y = event.target.y;
						currentOverlayShape.visible = true;
					}
					break;
					
				case MouseEvent.MOUSE_OUT:
					trace("CityGraphics Mouse MOUSE_OUT on:" +event.target);
					break;
					
				case MouseEvent.ROLL_OVER:
					trace("CityGraphics Mouse ROLL_OVER on:" +event.target);
					break;
					
				case MouseEvent.ROLL_OUT:
					trace("CityGraphics Mouse ROLL_OUT on:" +event.target);
					currentOverlayShape.visible = false;
					break;
					
				default:
					trace("Odd MouseEvent happened!");
					break;
			}
		}
		
		private function createGraphics():void
		{
			trace("Creating CityGraphics!");
			
			//Remove previous graphics:
			while (this.numChildren > 0)
			{
				this.removeChildAt(this.numChildren -1);
			}
			
			gridLines = new Sprite();
			gridLines.graphics.lineStyle(1, gridColor);
			gridLines.graphics.moveTo(0, Reg.gameHeight * gridSize);
			gridLines.graphics.lineTo(0, 0);
			gridLines.graphics.lineTo(Reg.gameWidth * gridSize, 0);
			
			var tempGrid:GridGraphics;
			for (var i:int = 0; i < Reg.gameWidth; i++) 
			{
				for (var j:int = 0; j < Reg.gameHeight; j++) 
				{
					var readBlock:Block = Reg.blockData[i][j];
					tempGrid = new GridGraphics();
					tempGrid.InitGrid(gridSize);
					tempGrid.x = i * gridSize;
					tempGrid.y = j * gridSize;
					
					switch(readBlock.type)
					{
						case "Empty":
							tempGrid.InitTextures(new TextureRock(), new TextureMetal());
							break;
							
						case "Residential":
							tempGrid.InitTextures(new TextureRock(), new TextureBlockResidential());
							break;
							
						case "Industry":
							tempGrid.InitTextures(new TextureRock(), new TextureBlockIndustrial());
							break;
							
						case "Police":
							tempGrid.InitTextures(new TextureRock(), new TextureBlockPolice());
							break;
							
						case "Slum":
							tempGrid.InitTextures(new TextureRock(), new TextureBlockSlum());
							break;
							
						case "TownHall":
							tempGrid.InitTextures(new TextureRock(), new TextureBlockTownhall());
							break;
							
						default:
							trace("WRONG BLOCK TYPE FOUND!");
							break;
					}
					
					this.addChild(tempGrid);
					
					gridLines.graphics.moveTo(tempGrid.x + gridSize, tempGrid.y);
					gridLines.graphics.lineTo(tempGrid.x + gridSize, tempGrid.y + gridSize);
					gridLines.graphics.lineTo(tempGrid.x, tempGrid.y + gridSize);
				}
			}
			
			this.addChild(gridLines);
			gridLines.visible = bShowGridlines;
			
			
			hightlightShape = new Shape();
			hightlightShape.graphics.lineStyle(4, 0xFFFFFF);
			hightlightShape.graphics.moveTo(0, 0);
			hightlightShape.graphics.lineTo(0, gridSize);
			hightlightShape.graphics.lineTo(gridSize, gridSize);
			hightlightShape.graphics.lineTo(gridSize, 0);
			hightlightShape.graphics.lineTo(0, 0);
			hightlightShape.visible = false;
			this.addChild(hightlightShape);
			
			currentOverlayShape = hightlightShape;
		} 
		
		private function updateGraphics():void
		{
			//TODO
		}

	}

}
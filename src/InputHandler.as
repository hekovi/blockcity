package  
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	import flash.geom.Point;
	
	import GridGraphics;
	/**
	 * ...
	 * @author Henrik
	 */
	public class InputHandler extends Sprite
	{
		private var parentMain:DisplayObject;
		
		public function InputHandler(inParent:DisplayObject, inEnabledUponCreation:Boolean = false) 
		{
			if (inParent != null)
			{
				parentMain = inParent;
				if (inEnabledUponCreation)
				{
					EnableInput(true);
				}
			}
		}
		
		public function EnableInput(inEnable:Boolean):void
		{
			if (inEnable)
			{
				parentMain.stage.addEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				parentMain.stage.addEventListener(MouseEvent.MOUSE_UP, handleMouse);
				parentMain.stage.addEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
			}
			else
			{
				parentMain.stage.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				parentMain.stage.removeEventListener(MouseEvent.MOUSE_UP, handleMouse);
				parentMain.stage.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
			}
		}
		
		private function handleMouse(event:MouseEvent):void
		{
			//trace("Handling mouse event for: "+event.target +" of type: "+event.type);
			switch (event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					trace("Mouse down on:" +event.target);
					break;
					
				case MouseEvent.MOUSE_UP:
					trace("Mouse up on:" +event.target);
					break;
					
				case MouseEvent.MOUSE_MOVE:
					/*
					var objects:Array = this.getObjectsUnderPoint(new Point(parentMain.stage.mouseX, parentMain.stage.mouseY));
					for (var i:int = 0; i< objects.length; i++)
					{
						trace(">>", objects[i].name,": ",objects[i]);
					}
					//trace("Mouse move");
					
					if (event.target is GridGraphics)
					{
						(event.target as GridGraphics).DrawHighlight(true);
					}
					*/
					break;
					
				case MouseEvent.ROLL_OVER:
					trace("Mouse ROLL_OVER");
					break;
					
				case MouseEvent.ROLL_OUT:
					trace("Mouse ROLL_OUT");
					break;
					
				default:
					trace("Odd event happened!");
					break;
			}
		}
		
	}

}
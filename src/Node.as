package  
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Emily
	 * 
	 * This acts exactly as a Point, except being able to store its parent node.
	 */
	public class Node extends Point
	{
		public var parentNode:Node;
		
		public function Node(X:int, Y:int, parentNode:Node = null) 
		{
			this.x = X;
			this.y = Y;
			this.parentNode = parentNode;
		}
		
	}

}
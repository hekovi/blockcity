package  
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Emily
	 */
	public class CityData
	{
		
		public function CityData() 
		{
			Reg.gameWidth = 10;
			Reg.gameHeight = 7;
			
			blankMap();
			
			Reg.blockData[4][4] = new Block(4, 4, "Residential", 1);
			Reg.blockData[4][5] = new Block(4, 5, "Residential", 1);
			Reg.blockData[4][6] = new Block(4, 6, "Residential", 1);
			Reg.blockData[4][7] = new Block(4, 7, "Residential", 1);
			Reg.blockData[5][5] = new Block(5, 5, "Residential", 1);
			Reg.blockData[6][5] = new Block(6, 5, "Residential", 1);
			Reg.blockData[6][6] = new Block(6, 6, "Residential", 1);
			//*/
			//Reg.blockData[7][6] = new Block(7, 5, "Residential", 1);
			//Reg.blockData[7][7] = new Block(6, 5, "Residential", 1);
			
			checkSurroundings(Reg.blockData[4][4]);
		}
		
		public function blankMap():void
		{
			Reg.blockData = new Array(Reg.gameWidth);
			
			for (var b:int = 0; b < Reg.gameWidth; b++ ) {
				Reg.blockData[b] = new Array(Reg.gameHeight)
			}
			
			for (var i:int = 0; i < Reg.gameWidth; i++) {
				for (var j:int = 0; j < Reg.gameHeight; j++) {
					Reg.blockData[i][j] = new Block(i, j, "Empty", 0);
				}
			}
		}
		
		public function clearBlock(X:int, Y:int):void
		{
			Reg.blockData[X][Y] = new Block(X, Y, "Empty", 0);
		}
		
		public function placeBlock(X:int, Y:int, block:Block):void
		{
			Reg.blockData[X][Y] = block;
			checkSurroundings(block);
		}
		
		private function checkSurroundings(block:Block):void 
		{
			/* In here The freshly placed block will find out what modifiers its neighbours bestow upon it.
			 * 
			 * 
			 * 
			 */
			
			var X:int = block.gridLocation.x;
			var Y:int = block.gridLocation.y;
			
			var chain:Vector.<Node> =  new Vector.<Node>();
			
			block.checkConnectionChain(chain);
			
			if (chain.length >= 3) {
				//finds all the connected blocks and removes themf rom the Reg.blockData[][] - need a way to trigger redrawing of these tiles.
				removeNeighbours(block, chain);
				
				//levels up block.
				block.levelUp();
			}
			
		}
		
		private function removeNeighbours(block:Block, chain:Vector.<Node>):void 
		{
			//var index:int = 0;
			var node:Node;
			
			while (chain.length > 1) {
				
				//fetches the last node in the chain, as the block thats being kept should be chain[0]
				node = chain[chain.length-1]
				if (node.x == block.gridLocation.x && node.y == block.gridLocation.y) {
					//do nothing
					trace("this is the placed tile" + node.x + "/" + node.y);
				}
				else {
					trace("This needs to be removed" + node.x + "/" + node.y);
					clearBlock(node.x, node.y);
					chain.splice(chain.length-1, 1);
				}
			}
			
		}
	}

}
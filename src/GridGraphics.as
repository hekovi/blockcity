package  
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Henrik
	 */
	public class GridGraphics extends MovieClip
	{
		
		private var backgroundTexture:Bitmap;
		private var buildingTexture:Bitmap;
		private var gridSize:Number;
		private var hightlightShape:Shape;
		
		public function GridGraphics():void
		{
			this.mouseChildren = false;
		}
		
		public function InitGrid(inGridSize:Number):void
		{
			gridSize = inGridSize;
		}
		
		public function InitTextures(inBackGroundTexture:Bitmap, inBuildingTexture:Bitmap):void
		{
			backgroundTexture = inBackGroundTexture;
			buildingTexture = inBuildingTexture;
			
			backgroundTexture.width = gridSize;
			backgroundTexture.height = gridSize;
			this.addChild(backgroundTexture);
			
			buildingTexture.width = gridSize * 0.5;
			buildingTexture.height = gridSize * 0.5;
			buildingTexture.x = gridSize * 0.25;
			buildingTexture.y = gridSize * 0.25;
			this.addChild(buildingTexture);
		}
		
	}

}
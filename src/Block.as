package  
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Emily
	 */
	public class Block 
	{
		public var gridLocation:Point;
		public var type:String; // "Residential", "Industry", "Police", "Slum", "Town Hall"; 
		public var level:int;
		
		public var g_wealth:Number; //generated wealth per "turn" or "hour" w/e
		public var g_happiness:Number; //possibly generated happiness per turn? does it generate or is it a fixed one-time effect?
		
		
		public var e_score:Number; //score for end tallying? Maybe save in a 2d array by [type][level] instead though.
		
		
		public function Block(X:int, Y:int, type:String, level:int) 
		{
			gridLocation = new Point(X,Y);
			this.type = type;
			this.level = level;
		}
		
		public function levelUp():void
		{
			this.level++;
		}
		
		public function changeType(inNewType:String):void
		{
			type = inNewType;
			trace("Changed Block's type to: " + Reg.selectedBlockType);
			//TODO: Everything else what happens when the type changes!
			//		Adjacent Blocks changing etc.
			
			//		There's a function in cityData that's "placeBlock" Is it better to have it there, or to just change the type 
			//		using this here? - I guess so?
			// 		In future there will be all the values that need to change so I was thinking some kind of look-up table would be useful?
		}
		
		public function addConnections(chain:Vector.<Node>, lastNode:Node = null, node:Node = null):void 
		{
			//not done anything ehre.!!
			//
			//
			if (lastNode == null) {
				lastNode = new Node(this.gridLocation.x, this.gridLocation.y, null);
			}
			//chain.push(lastNode);
			var connections:Vector.<Node> = new Vector.<Node>();
			
		}
	
		
		public function checkConnectionChain(connections:Vector.<Node>, node:Node = null, lastNode:Node = null):Vector.<Node>
		{
			//this one retursn a chain of nodes - though right now it returns several nodes, this could be rewritten to return just a chain of pure Points.
			if (node == null) {
				node = new Node(this.gridLocation.x, this.gridLocation.y);
			}
			//check if it's checking the previous connection
			if (lastNode != null &&  lastNode.parentNode != null &&
				node.x == lastNode.parentNode.x && node.y == lastNode.parentNode.y){
					return null;
			}
			//check if it's trying to go out of bounds. if it's 
			/*
			if (node.x <= 0 && 
				node.x >= Reg.gameWidth - 1 && 
				node.y <= 0 && 
				node.y >= Reg.gameHeight - 1)	{
					return null;
			}*/
			
			if (lastNode != null) {
				node.parentNode = lastNode;
			}
			
			//add the node.
			connections.push(node);
			
			//now check all the horizontal and vertical directions. (not diagonals)
			if (!(node.x + 1 <= Reg.gameWidth - 1)) {
				if (Reg.blockData[node.x + 1][node.y].type == type && 
					Reg.blockData[node.x + 1][node.y].level == level) { //check right
					//connections = new Node(node.x +1, node.y, node)
						checkConnectionChain(connections, new Node(node.x + 1, node.y, node), node);
				}
			}
			if (!(node.x - 1 <= 0)) {
				if (Reg.blockData[node.x - 1][node.y].type == type && 
					Reg.blockData[node.x - 1][node.y].level == level) {//check left
						checkConnectionChain(connections, new Node(node.x - 1, node.y, node), node);
				}
			}
			if (!(node.y - 1 <= 0)) {
				if (Reg.blockData[node.x][node.y - 1].type == type && 
					Reg.blockData[node.x][node.y -1].level == level) {//check up
						checkConnectionChain(connections, new Node(node.x, node.y - 1, node), node);
				}
			}
			if (!(node.y + 1 >= Reg.gameHeight - 1)) {
				if (Reg.blockData[node.x][node.y + 1].type == type && 
					Reg.blockData[node.x][node.y +1].level == level) {//check down
						checkConnectionChain(connections, new Node(node.x, node.y + 1, node), node);
					}
			}
			trace("Found " + connections.length + " connected blocks");
			return connections;
		}
		
		/*
		public function checkConnections(point:Node = null, lastpoint:Node = null):int
		{
			var connections:int;
			if (point == null) {
				point = new Node(gridLocation.x, gridLocation.y, null);
			}
			//check if it's checking the previous connection
			if (lastpoint != null && point.x == lastpoint.parentNode.x && point.y == lastpoint.parentNode.y){
				return 0;
			}
			//check if it's trying to go out of bounds.
			if (point.x <= 0 && 
				point.x >= Reg.width - 1 && 
				point.y <= 0 && 
				point.y >= Reg.height - 1)	{
					return 0;
			}
			
			connections++;
			//now check all the horizontal and vertical directions. (not diagonals)
			if (Reg.blockData[point.x + 1][point.y].type == type && Reg.blockData[point.x + 1][point.y].level == level) { //check right
				connections += checkConnections(new Node(point.x + 1, point.y), point);
			}
			if (Reg.blockData[point.x - 1][point.y].type == type && Reg.blockData[point.x - 1][point.y].level == level) {//check left
				connections += checkConnections(new Node(point.x - 1, point.y), point);
			}
			if (Reg.blockData[point.x][point.y - 1].type == type && Reg.blockData[point.x][point.y -1].level == level) {//check up
				connections += checkConnections(new Node(point.x, point.y - 1), point);
			}
			if (Reg.blockData[point.x][point.y + 1].type == type && Reg.blockData[point.x][point.y +1].level == level) {//check down
				connections += checkConnections(new Node(point.x, point.y + 1), point);
			}
			return connections;
		}*/
	}

}
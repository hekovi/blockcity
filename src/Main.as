package 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import CityGraphics;
	import CityData;
	import InputHandler;
	import GameHUD;
	
	[SWF(width="1024", height="768", backgroundColor="#000000")]
	/**
	 * ...
	 * @author Henrik
	 */
	public class Main extends Sprite 
	{		
		//consts:
		private const citySizeX:uint = 14;
		private const citySizeY:uint = 10;
		
		//private vars:
		private var cityData:CityData;
		private var cityGraphics:CityGraphics;
		private var inputHandler:InputHandler;
		private var hud:GameHUD;
		
		//public vars:
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			createCity();
			createUI();
		}
		
		private function createCity():void
		{
			Reg.width = citySizeX;
			Reg.height = citySizeY;
			
			//City Data:
			cityData = new CityData();
			
			//City Graphics
			cityGraphics = new CityGraphics();
			stage.addChild(cityGraphics);
			cityGraphics.Init();
			
		}
		
		private function createUI():void
		{
			hud = new GameHUD();
			stage.addChild(hud);
			hud.Init();
		}
		
	}
	
}
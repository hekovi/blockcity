package  
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Henrik
	 */
	public class GameHUD extends MovieClip
	{
		private const hudHeight:Number = 128.0;
		
		//UI Elements:
		private var bgSprite:Sprite;
		private var Empty:HUDButton;
		private var Residential:HUDButton;
		private var Industry:HUDButton;
		private var Police:HUDButton;
		private var Slum:HUDButton;
		
		public function GameHUD() 
		{

		}
		
		public function Init():void
		{
			bgSprite = new Sprite();
			bgSprite.graphics.lineStyle(2,0x00ff00);
			bgSprite.graphics.beginFill(0xEEEEEE);
			bgSprite.graphics.drawRect(0,0,stage.stageWidth, hudHeight);
			bgSprite.graphics.endFill();
			bgSprite.x = 0.0;
			bgSprite.y = stage.stageHeight - hudHeight;
			
			this.addChild(bgSprite);
			
			createButtons();
			
			EnableHUDInput(true);
		}
		
		private function createButtons():void
		{
			Empty = new HUDButton(0xCCCCCC);
			Empty.x = 60.0;
			Empty.y = bgSprite.y + 20.0;
			this.addChild(Empty);
			
			Residential = new HUDButton(0x00DD00);
			Residential.x = 260.0;
			Residential.y = bgSprite.y + 20.0;
			this.addChild(Residential);
			
			Industry = new HUDButton(0xDDDD00);
			Industry.x = 460.0;
			Industry.y = bgSprite.y + 20.0;
			this.addChild(Industry);
			
			Police = new HUDButton(0x0000DD);
			Police.x = 660.0;
			Police.y = bgSprite.y + 20.0;
			this.addChild(Police);
			
			Slum = new HUDButton(0x222222);
			Slum.x = 860.0;
			Slum.y = bgSprite.y + 20.0;
			this.addChild(Slum);
		}
		
		public function EnableHUDInput(inEnable:Boolean):void
		{
			if (inEnable)
			{
				this.addEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_UP, handleMouse);
				this.addEventListener(MouseEvent.CLICK, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_OVER, handleMouse);
				this.addEventListener(MouseEvent.MOUSE_OUT, handleMouse);
				this.addEventListener(MouseEvent.ROLL_OVER, handleMouse);
				this.addEventListener(MouseEvent.ROLL_OUT, handleMouse);
			}
			else
			{
				this.removeEventListener(MouseEvent.MOUSE_DOWN, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_UP, handleMouse);
				this.removeEventListener(MouseEvent.CLICK, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_MOVE, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_OVER, handleMouse);
				this.removeEventListener(MouseEvent.MOUSE_OUT, handleMouse);
				this.removeEventListener(MouseEvent.ROLL_OVER, handleMouse);
				this.removeEventListener(MouseEvent.ROLL_OUT, handleMouse);
			}
		}
		
		private function handleMouse(event:MouseEvent):void
		{
			//trace("Handling mouse event for: "+event.target +" of type: "+event.type);
			switch (event.type)
			{
				case MouseEvent.MOUSE_DOWN:
					trace("GameHUD Mouse down on:" +event.target);
					break;
					
				case MouseEvent.MOUSE_UP:
					trace("GameHUD Mouse up on:" +event.target);
					break;
					
				case MouseEvent.CLICK:
					trace("GameHUD Mouse CLICK on:" +event.target);
					if (event.target is HUDButton)
					{
						switch(event.target)
						{
							case Empty:
								Reg.selectedBlockType = "Empty";
								break;
							case Residential:
								Reg.selectedBlockType = "Residential";
								break;
							case Industry:
								Reg.selectedBlockType = "Industry";
								break;
							case Police:
								Reg.selectedBlockType = "Police";
								break;
							case Slum:
								Reg.selectedBlockType = "Slum";
								break;
						}
						trace("Set Reg.selectedBlockType to: " + Reg.selectedBlockType);
					}
					break;
					
				case MouseEvent.MOUSE_MOVE:
					break;
					
				case MouseEvent.MOUSE_OVER:
					trace("GameHUD Mouse MOUSE_OVER on:" +event.target);
					break;
					
				case MouseEvent.MOUSE_OUT:
					trace("GameHUD Mouse MOUSE_OUT on:" +event.target);
					break;
					
				case MouseEvent.ROLL_OVER:
					trace("GameHUD Mouse ROLL_OVER on:" +event.target);
					break;
					
				case MouseEvent.ROLL_OUT:
					trace("GameHUD Mouse ROLL_OUT on:" +event.target);
					break;
					
				default:
					trace("Odd MouseEvent happened!");
					break;
			}
		}
		
	}

}